#!/usr/bin/env bash
# bootstrap.sh

set -euo pipefail

readonly REPO_URL="https://gitlab.com/cookieinajar/aimee"
readonly REPO_DIR="${PWD}/aimee"
readonly PACMAN_INSTALL_CMD="pacman -Sy --noconfirm"
readonly LOG_FILE="/var/log/aimee_bootstrap.log"

log() {
    local level="$1"
    local message="$2"
    echo "$message"
    printf "[%s] [%s] %s\n" "$(date +'%Y-%m-%d %H:%M:%S')" "$level" "$message" >> "$LOG_FILE"
}

log_info() { log "INFO" "$1"; }
log_error() { log "ERROR" "$1" >&2; }

command_exists() {
    command -v "$1" >/dev/null 2>&1
}

install_if_not_exists() {
    local package="$1"
    if ! command_exists "$package"; then
        log_info "$package is not installed. Attempting to install $package..."
        if ! $PACMAN_INSTALL_CMD "$package"; then
            log_error "Failed to install $package. Please install it manually and try again."
            exit 1
        fi
    fi
}

check_root() {
    if [[ $EUID -ne 0 ]]; then
        log_error "This script must be run as root" 
        exit 1
    fi
}

check_internet() {
    if ! ping -c 1 gitlab.com &> /dev/null; then
        log_error "No internet connection. Please connect to the internet and try again."
        exit 1
    fi
}

clone_or_update_repo() {
    if [[ -d "$REPO_DIR" ]]; then
        log_info "Updating existing repository..."
        if ! (cd "$REPO_DIR" && git fetch origin && git reset --hard origin/master); then
            log_error "Failed to update repository"
            exit 1
        fi
    else
        log_info "Cloning repository..."
        if ! git clone "$REPO_URL" "$REPO_DIR"; then
            log_error "Failed to clone repository"
            exit 1
        fi
    fi
}

make_scripts_executable() {
    if ! chmod +x "$REPO_DIR"/src/*.sh; then
        log_error "Failed to make scripts executable"
        exit 1
    fi
}

main() {
    check_root
    check_internet

    install_if_not_exists gum
    install_if_not_exists git

    # if ! gum confirm "Do you want to proceed with the installation?"; then
    #    log_info "Installation cancelled by user"
    #    exit 0
    # fi

    clone_or_update_repo
    make_scripts_executable

    log_info "Installation files are ready."
    log_info "To start the installation, run:"
    log_info "  $REPO_DIR/src/main.sh [options]"
    log_info "For available options, run:"
    log_info "  $REPO_DIR/src/main.sh --help"
    log_info "Log file location: $LOG_FILE"
}

main "$@"