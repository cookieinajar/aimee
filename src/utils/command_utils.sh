#!/usr/bin/env bash
# src/utils/command_utils.sh

run_command() {
    local cmd_description="$1"
    shift

    log_debug "Executing: $cmd_description"
    log_debug "Command: $*"

    local output
    if ! output=$("$@" 2>&1); then
        log_error "Command failed: $cmd_description"
        log_error "Command: $*"
        log_error "Output: $output"
        return 1
    fi

    log_debug "Command succeeded: $cmd_description"
    log_debug "Output: $output"
    echo "$output"
    return 0
}