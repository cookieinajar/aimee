#!/usr/bin/env bash
# src/utils/logger.sh

# Logging levels
readonly DEBUG=0
readonly INFO=1
readonly WARN=2
readonly ERROR=3
readonly FATAL=4

# Convert log level to numeric value
log_level_to_number() {
    case "${1,,}" in
        debug) echo "$DEBUG" ;;
        info)  echo "$INFO" ;;
        warn)  echo "$WARN" ;;
        error) echo "$ERROR" ;;
        fatal) echo "$FATAL" ;;
        *)     echo "$INFO" ;;  # Default to INFO
    esac
}

# Validate and set default log level
validate_log_level() {
    LOG_LEVEL=$(log_level_to_number "${LOG_LEVEL:-info}")
}

# Main logging function
log() {
    local level_str="${1:-}"
    local message="${2:-}"
    shift 2
    local level_num

    level_num=$(log_level_to_number "$level_str")
    
    if [[ -z "$message" ]]; then
        ui_log "error" "No message provided for logging"
        printf "[%s] [ERROR] No message provided for logging\n" "$(date +"%Y-%m-%d %H:%M:%S")" >> "${LOG_FILE:-/dev/null}"
        return 1
    fi

    local timestamp
    timestamp=$(date +"%Y-%m-%d %H:%M:%S")

    # Always log to file
    if [[ -n "${LOG_FILE:-}" ]]; then
        printf "[%s] [%s] %s\n" "$timestamp" "${level_str^^}" "$message" >> "$LOG_FILE"
        if (( $# > 0 )); then
            printf "Additional info: %s\n" "$*" >> "$LOG_FILE"
        fi
    fi

    # Only log to terminal if level is high enough
    if (( level_num >= LOG_LEVEL )); then
        ui_log "$level_str" "$message" "$@"
    fi
}

# Convenience functions for each log level
log_debug() { log "debug" "$@"; }
log_info()  { log "info"  "$@"; }
log_warn()  { log "warn"  "$@"; }
log_error() { log "error" "$@"; }
log_fatal() { log "fatal" "$@"; }

# Initialize logging
validate_log_level