#!/usr/bin/env bash
# src/utils/ui.sh

# Color definitions
readonly UI_COLOR_PRIMARY=212

ui_show_ascii_art() {
    gum style --foreground "$UI_COLOR_PRIMARY" "$(cat << "EOF"
▄███████████████████████████████████████████████▄
██████ █████▄  ▄█▄  ████  ▄█▄  ▄▄▄▄ █▄  ▄▄▄▄ ████
█████   █████  ███   ██   ███  ███████  ██▀██████
████ ██  ████  ███ ▄ ▀▀▄  ███  ▀▀ ████  ▄▄ ██████
███▀▄▄▄▄ ████  ███ █▄ ▄█  ███  ██▄████  █████████
██▀ ▀██▀  ▀█▀  ▀█▀ ▀█ █▀  ▀█▀  ▀▀▀▀ █▀  ▀▀▀▀▀ ███
█████████████████████████████████████████████████
▀█▓▒░Arch Installer Might Eventually Execute░▒▓█▀
  ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀
EOF
)"
}

ui_welcome() {
    ui_show_ascii_art
    ui_section "Welcome to the Arch Linux Installer"
}

ui_confirm() {
    gum confirm "$1"
}

ui_installation_complete() {
    ui_section "Installation Complete!"
}

ui_section() {
    local title="$1"
    gum style --border normal --margin "1" --padding "1 2" --border-foreground "$UI_COLOR_PRIMARY" "$title"
}

ui_input() {
    local prompt="$1"
    local placeholder="${2:-}"
    gum input --placeholder="$placeholder" --header="$prompt"
}

ui_password() {
    local prompt="$1"
    gum input --password --header="$prompt"
}

ui_choose() {
    local prompt="$1"
    shift
    gum choose --header="$prompt" "$@"
}

ui_progress() {
    local title="$1"
    shift
    gum spin --spinner dot --title "$title" -- "$@"
}

ui_log() {
    local level="$1"
    local message="$2"
    shift 2
    gum log --structured --level "$level" "$message" "$@"
}