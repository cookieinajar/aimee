#!/usr/bin/env bash
# src/modules/02_disk_setup.sh

set -eo pipefail

# Default configurations
readonly DEFAULT_EFI_SIZE="512M"
readonly DEFAULT_SWAP_SIZE="4G"

select_disk() {
    local disk disks
    disk=$(get_config "selected_disk")
    
    if [[ -z "$disk" ]]; then
        mapfile -t disks < <(lsblk -dpno NAME,SIZE,MODEL | grep -v "loop\|sr")
        
        if [[ ${#disks[@]} -eq 0 ]]; then
            log_error "No disks found"
            return 1
        elif [[ ${#disks[@]} -eq 1 ]]; then
            disk="${disks[0]%% *}"
        else
            disk=$(gum choose --header="Select the disk to partition:" "${disks[@]}" | awk '{print $1}')
        fi
        
        save_config "selected_disk" "$disk"
    fi
    
    if ! lsblk "$disk" &>/dev/null; then
        log_error "Selected disk $disk is not available"
        save_config "selected_disk" ""
        return 1
    fi
    
    echo "$disk"
}

customize_sizes() {
    local disk="$1"

    EFI_SIZE=$(gum input --value "$DEFAULT_EFI_SIZE" --placeholder "EFI partition size (e.g., 512M)")
    SWAP_SIZE=$(gum input --value "$DEFAULT_SWAP_SIZE" --placeholder "Swap partition size (e.g., 4G)")
    
    gum style --border normal --margin "1" --padding "1 2" \
        "EFI Size: $EFI_SIZE" \
        "Swap Size: $SWAP_SIZE" \
        "Root: Remaining space"

    if ! gum confirm "Are these sizes okay?"; then
        customize_sizes "$disk"
    fi
}

select_filesystem() {
    FILESYSTEM=$(gum choose --header="Select filesystem for root partition:" "btrfs" "ext4")
}

partition_disk() {
    local disk="$1"
    
    if [[ "$(get_config "disk_partitioned")" == "true" ]]; then
        log_info "Disk $disk is already partitioned"
        return 0
    fi

    log_info "Partitioning disk $disk"

    run_command "Create GPT partition table" parted -s "$disk" mklabel gpt
    run_command "Create EFI partition" parted -s "$disk" mkpart ESP fat32 1MiB "$EFI_SIZE"
    run_command "Set ESP flag" parted -s "$disk" set 1 esp on
    run_command "Create swap partition" parted -s "$disk" mkpart primary linux-swap "$EFI_SIZE" "$SWAP_SIZE"
    run_command "Create root partition" parted -s "$disk" mkpart primary "$SWAP_SIZE" 100%

    save_config "disk_partitioned" "true"
    log_info "Disk $disk has been partitioned"
}

format_partitions() {
    local disk="$1"

    if [[ "$(get_config "partitions_formatted")" == "true" ]]; then
        log_info "Partitions on $disk are already formatted"
        return 0
    fi

    run_command "Format EFI partition" mkfs.fat -F32 "${disk}1"
    run_command "Format swap partition" mkswap "${disk}2"
    
    if [[ "$FILESYSTEM" == "btrfs" ]]; then
        run_command "Format root partition" mkfs.btrfs -f "${disk}3"
    else
        run_command "Format root partition" mkfs.ext4 "${disk}3"
    fi

    save_config "partitions_formatted" "true"
    log_info "Partitions on $disk have been formatted"
}

create_btrfs_subvolumes() {
    local disk="$1"

    if [[ "$(get_config "subvolumes_created")" == "true" ]]; then
        log_info "BTRFS subvolumes are already created"
        return 0
    fi

    run_command "Mount root filesystem" mount "${disk}3" /mnt

    run_command "Create @ subvolume" btrfs subvolume create /mnt/@
    run_command "Create @home subvolume" btrfs subvolume create /mnt/@home
    run_command "Create @snapshots subvolume" btrfs subvolume create /mnt/@snapshots

    run_command "Unmount root filesystem" umount /mnt

    save_config "subvolumes_created" "true"
    log_info "BTRFS subvolumes have been created"
}

mount_filesystems() {
    local disk="$1"

    if [[ "$(get_config "filesystems_mounted")" == "true" ]]; then
        log_info "Filesystems are already mounted"
        return 0
    fi

    if [[ "$FILESYSTEM" == "btrfs" ]]; then
        run_command "Mount root subvolume" mount -o subvol=@,compress=zstd "${disk}3" /mnt
        run_command "Create mount points" mkdir -p /mnt/{home,boot/efi,.snapshots}
        run_command "Mount home subvolume" mount -o subvol=@home,compress=zstd "${disk}3" /mnt/home
        run_command "Mount snapshots subvolume" mount -o subvol=@snapshots,compress=zstd "${disk}3" /mnt/.snapshots
    else
        run_command "Mount root partition" mount "${disk}3" /mnt
        run_command "Create boot/efi directory" mkdir -p /mnt/boot/efi
    fi

    run_command "Mount EFI partition" mount "${disk}1" /mnt/boot/efi
    run_command "Enable swap" swapon "${disk}2"

    save_config "filesystems_mounted" "true"
    log_info "Filesystems have been mounted"
}

verify_disk_setup() {
    local disk="$1"
    local all_verified=true

    log_info "Verifying disk setup for $disk"

    # Check partitions
    for i in 1 2 3; do
        if [[ ! -e "${disk}${i}" ]]; then
            log_error "Partition ${disk}${i} does not exist"
            all_verified=false
        fi
    done

    # Check filesystems
    if ! blkid -t TYPE=vfat "${disk}1" &>/dev/null; then
        log_error "EFI partition (${disk}1) is not formatted as FAT32"
        all_verified=false
    fi
    if ! blkid -t TYPE=swap "${disk}2" &>/dev/null; then
        log_error "Swap partition (${disk}2) is not formatted as swap"
        all_verified=false
    fi
    if ! blkid -t TYPE="$FILESYSTEM" "${disk}3" &>/dev/null; then
        log_error "Root partition (${disk}3) is not formatted as $FILESYSTEM"
        all_verified=false
    fi

    # Check mount points
    local mount_points=("/mnt" "/mnt/boot/efi")
    [[ "$FILESYSTEM" == "btrfs" ]] && mount_points+=("/mnt/home" "/mnt/.snapshots")
    
    for mount_point in "${mount_points[@]}"; do
        if ! mountpoint -q "$mount_point"; then
            log_error "Mount point $mount_point is not mounted"
            all_verified=false
        fi
    done

    # Check swap
    if ! swapon --show | grep -q "${disk}2"; then
        log_error "Swap is not activated on ${disk}2"
        all_verified=false
    fi

    if [[ "$all_verified" == "true" ]]; then
        log_info "Disk setup verification completed successfully"
        return 0
    else
        log_error "Disk setup verification failed"
        return 1
    fi
}

disk_setup() {
    ui_section "Disk Setup"

    local current_step disk
    current_step=$(get_install_step)
    
    if [[ "$current_step" -lt 2 ]]; then
        disk=$(select_disk) || return 1

        if ! gum confirm "Are you sure you want to set up $disk? This will erase all data on the disk."; then
            log_info "Disk setup cancelled by user"
            return 1
        fi

        customize_sizes "$disk"
        select_filesystem

        partition_disk "$disk" || return 1
        format_partitions "$disk" || return 1
        if [[ "$FILESYSTEM" == "btrfs" ]]; then
            create_btrfs_subvolumes "$disk" || return 1
        fi
        mount_filesystems "$disk" || return 1

        if verify_disk_setup "$disk"; then
            log_info "Disk setup completed and verified successfully"
            update_install_step 2
        else
            log_error "Disk setup failed verification"
            return 1
        fi
    else
        log_info "Disk setup already completed, verifying..."
        disk=$(get_config "selected_disk")
        if ! verify_disk_setup "$disk"; then
            log_error "Disk setup verification failed. Please run the script again."
            return 1
        fi
    fi

    log_info "Disk setup completed successfully"
}