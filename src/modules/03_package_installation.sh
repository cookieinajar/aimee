#!/usr/bin/env bash
# src/modules/05_package_installation.sh

ESSENTIAL_PACKAGES=(
    base
    linux
    linux-firmware
    base-devel
    sudo
    networkmanager
    grub
    efibootmgr
    btrfs-progs
    nano
    man-db
    man-pages
    git
    reflector
)

RECOMMENDED_PACKAGES=(
    # Audio
    pipewire
    pipewire-alsa
    pipewire-pulse
    pipewire-jack

    # BTRFS utilities
    grub-btrfs
    inotify-tools

    # Network utilities
    wget
    curl
    openssh

    # System utilities
    btop
    neofetch
    tree
    usbutils
    pciutils

    # Compression tools
    zip
    unzip

    # Text editors
    vim

    # Shell enhancements
    bash-completion

    # Hardware info and management
    lm_sensors

    # Package management helpers
    pacman-contrib
)

LAPTOP_PACKAGES=(
    acpi
    tlp
)

detect_system_type() {
    # Check for battery
    if compgen -G "/sys/class/power_supply/BAT*" > /dev/null; then
        echo "laptop"
        return 0
    fi

    # Check for lid
    if [ -d "/proc/acpi/button/lid" ]; then
        echo "laptop"
        return 0
    fi

    # Check DMI chassis type
    if [ -f "/sys/class/dmi/id/chassis_type" ]; then
        chassis_type=$(cat /sys/class/dmi/id/chassis_type)
        case $chassis_type in
            8|9|10|11|14)  # Various laptop types
                echo "laptop"
                return 0
                ;;
        esac
    fi

    # If none of the above checks passed, assume it's a desktop
    echo "desktop"
    return 1
}

detect_wifi() {
    if lspci | grep -qi network || lsusb | grep -qi network; then
        return 0
    else
        return 1
    fi
}

detect_bluetooth() {
    if lsusb | grep -qi bluetooth || hciconfig 2>/dev/null | grep -q .; then
        return 0
    else
        return 1
    fi
}

install_packages() {
    log_info "Installing essential packages"
    run_command pacstrap -K /mnt "${ESSENTIAL_PACKAGES[@]}"

    # Check CPU and install appropriate microcode
    if grep -q "AMD" /proc/cpuinfo; then
        run_command pacstrap -K /mnt amd-ucode
    elif grep -q "Intel" /proc/cpuinfo; then
        run_command pacstrap -K /mnt intel-ucode
    fi

    # Show recommended packages and ask for confirmation
    echo "Recommended packages:"
    printf '%s\n' "${RECOMMENDED_PACKAGES[@]}" | column

    if gum confirm "Would you like to install these recommended packages?"; then
        log_info "Installing recommended packages"
        run_command pacstrap -K /mnt "${RECOMMENDED_PACKAGES[@]}"

        # Laptop-specific packages
        if [[ $(detect_system_type) == "laptop" ]]; then
            log_info "Laptop detected. Installing laptop-specific packages:"
            printf '%s\n' "${LAPTOP_PACKAGES[@]}" | column
            run_command pacstrap -K /mnt "${LAPTOP_PACKAGES[@]}"
        fi

        # Wi-Fi packages
        if detect_wifi; then
            log_info "Wi-Fi capability detected. Installing iwd."
            run_command pacstrap -K /mnt iwd
        fi

        # Bluetooth packages
        if detect_bluetooth; then
            log_info "Bluetooth capability detected. Installing bluez and bluez-utils."
            run_command pacstrap -K /mnt bluez bluez-utils
        fi
    else
        log_info "Skipping recommended packages"
    fi

    # Prompt for additional packages
    local custom_packages
    custom_packages=$(gum input --placeholder "Enter additional packages (space-separated)" )

    if [[ -n "$custom_packages" ]]; then
        log_info "Installing custom packages: $custom_packages"
        run_command pacstrap -K /mnt "$custom_packages"
        save_config "custom_packages" "$custom_packages"
    fi
}

verify_package_installation() {
    local all_installed=true

    for package in "${ESSENTIAL_PACKAGES[@]}"; do
        if ! run_command arch-chroot /mnt pacman -Q "$package" &>/dev/null; then
            log_error "Essential package $package is not installed"
            all_installed=false
        fi
    done

    # Check microcode
    if grep -q "AMD" /proc/cpuinfo; then
        if ! run_command arch-chroot /mnt pacman -Q amd-ucode &>/dev/null; then
            log_error "AMD microcode is not installed"
            all_installed=false
        fi
    elif grep -q "Intel" /proc/cpuinfo; then
        if ! run_command arch-chroot /mnt pacman -Q intel-ucode &>/dev/null; then
            log_error "Intel microcode is not installed"
            all_installed=false
        fi
    fi

    if [[ "$(get_config "install_recommended")" == "yes" ]]; then
        for package in "${RECOMMENDED_PACKAGES[@]}"; do
            if ! run_command arch-chroot /mnt pacman -Q "$package" &>/dev/null; then
                log_warn "Recommended package $package is not installed"
            fi
        done

        if [[ $(detect_system_type) == "laptop" ]]; then
            for package in "${LAPTOP_PACKAGES[@]}"; do
                if ! run_command arch-chroot /mnt pacman -Q "$package" &>/dev/null; then
                    log_warn "Laptop package $package is not installed"
                fi
            done
        fi

        if detect_wifi; then
            if ! run_command arch-chroot /mnt pacman -Q iwd &>/dev/null; then
                log_warn "Wi-Fi package iwd is not installed"
            fi
        fi

        if detect_bluetooth; then
            if ! run_command arch-chroot /mnt pacman -Q bluez bluez-utils &>/dev/null; then
                log_warn "Bluetooth packages are not installed"
            fi
        fi
    fi

    # Check custom packages
    local custom_packages
    custom_packages=$(get_config "custom_packages")
    if [[ -n "$custom_packages" ]]; then
        for package in $custom_packages; do
            if ! run_command arch-chroot /mnt pacman -Q "$package" &>/dev/null; then
                log_warn "Custom package $package is not installed"
            fi
        done
    fi

    if [[ "$all_installed" == "true" ]]; then
        log_info "All essential packages are successfully installed"
        return 0
    else
        return 1
    fi
}

package_installation() {
    ui_section "Package Installation"

    local current_step
    current_step=$(get_install_step)

    if [[ "$current_step" -lt 3 ]]; then
        install_packages
        if verify_package_installation; then
            update_install_step 3
        else
            log_error "Package installation failed"
            return 1
        fi
    else
        log_info "Package installation already completed, verifying..."
        if ! verify_package_installation; then
            log_warn "Some packages are missing, reinstalling..."
            install_packages
            if ! verify_package_installation; then
                log_error "Package installation failed after retry"
                return 1
            fi
        fi
    fi

    log_info "Package installation completed successfully"
}