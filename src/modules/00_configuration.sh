#!/usr/bin/env bash
# src/modules/00_configuration.sh

# Configuration file path
readonly CONFIG_FILE="/tmp/arch_install_config"

# Save a key-value pair to the configuration file
save_config() {
    local key="$1" value="$2"
    if [[ -z "$key" || -z "$value" ]]; then
        log_error "save_config: Key or value is empty"
        return 1
    fi
    sed -i "/$key=/d" "$CONFIG_FILE" 2>/dev/null || true
    echo "$key=$value" >> "$CONFIG_FILE" || log_error "Failed to write to $CONFIG_FILE"
}

# Get a value from the configuration file
get_config() {
    local key="$1"
    if [[ -z "$key" ]]; then
        log_error "get_config: Key is empty"
        return 1
    fi
    grep "^$key=" "$CONFIG_FILE" 2>/dev/null | cut -d'=' -f2
}

# Initialize the configuration file
init_config() {
    if [[ ! -f "$CONFIG_FILE" ]]; then
        touch "$CONFIG_FILE" || log_error "Failed to create $CONFIG_FILE"
        save_config "install_step" "0"
        log_info "Configuration file initialized at $CONFIG_FILE"
    else
        log_info "Configuration file already exists at $CONFIG_FILE"
    fi
}

# Update the installation step
update_install_step() {
    local step="$1"
    if [[ -z "$step" ]]; then
        log_error "update_install_step: Step is empty"
        return 1
    fi
    save_config "install_step" "$step"
    log_info "Installation step updated to $step"
}

# Get the current installation step
get_install_step() {
    get_config "install_step"
}