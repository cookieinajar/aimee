#!/usr/bin/env bash
# src/modules/01_preliminary_setup.sh

set -eo pipefail

# Set the keyboard layout
set_keyboard_layout() {
    local layout layouts
    
    log_debug "Starting set_keyboard_layout function"
    
    layout=$(get_config "keyboard_layout")
    log_debug "Retrieved layout from config: '$layout'"
    
    if [[ -z "$layout" ]]; then
        log_debug "No layout in config, listing available layouts"
        if ! layouts=$(run_command "List keyboard layouts" localectl list-keymaps); then
            log_error "Failed to get list of keyboard layouts"
            return 1
        fi
        
        log_info "Please select a keyboard layout"
        layout=$(echo "$layouts" | tr ' ' '\n' | gum filter --placeholder "Search for your keyboard layout...")
        log_debug "User selected layout: '$layout'"
        
        if [[ -z "$layout" ]]; then
            log_warn "No keyboard layout selected. Using default (us)"
            layout="us"
        fi
        
        log_debug "Saving layout '$layout' to config"
        save_config "keyboard_layout" "$layout"
    fi

    log_debug "Attempting to set keyboard layout to '$layout'"
    if ! run_command "Set keyboard layout" loadkeys "$layout"; then
        log_error "Failed to set keyboard layout to $layout"
        return 1
    fi
    log_info "Keyboard layout set to $layout"
}

# Check internet connection
check_internet_connection() {
    local retry_count=0
    local max_retries=3

    while ! run_command "Check internet connection" ping -c 1 archlinux.org; do
        ((retry_count++))
        log_debug "Internet connection attempt $retry_count failed"
        if [[ $retry_count -ge $max_retries ]]; then
            log_error "No internet connection detected after $max_retries attempts"
            log_info "Please check your network settings and try again"
            exit 1
        fi
        log_warn "Internet connection not available. Retrying in 5 seconds... (Attempt $retry_count/$max_retries)"
        sleep 5
    done

    log_info "Internet connection is available"
}

# Update the system clock
update_system_clock() {
    log_debug "Updating system clock"
    run_command "Update system clock" timedatectl set-ntp true
    log_info "System clock updated"
}

# Check if running in UEFI mode
check_uefi_mode() {
    log_debug "Checking boot mode"
    if [[ -d "/sys/firmware/efi/efivars" ]]; then
        log_info "System is running in UEFI mode"
        save_config "boot_mode" "uefi"
    else
        log_info "System is running in BIOS mode"
        save_config "boot_mode" "bios"
    fi
}

# Verify system requirements
check_system_specifications() {
    local recommended_ram=2048 # Recommended RAM in MB (2 GB)

    log_debug "Checking system specifications"
    local total_ram
    total_ram=$(free -m | awk '/^Mem:/{print $2}')
    
    log_info "System specifications: RAM: $total_ram MB"

    if [[ $total_ram -lt $recommended_ram ]]; then
        log_warn "RAM is below recommended: $total_ram MB. Recommended: $recommended_ram MB"
        log_warn "The installation may proceed, but system performance might be affected."
        if ! ui_confirm "Do you want to continue with the installation?"; then
            log_info "Installation cancelled by user due to low RAM."
            exit 0
        fi
    fi

    log_debug "System specification check completed"
}

# Main function for preliminary setup
preliminary_setup() {
    ui_section "Preliminary Setup"
    
    local current_step
    current_step=$(get_install_step)
    log_debug "Current installation step: $current_step"
    
    if [[ "$current_step" -lt 1 ]]; then
        log_debug "Starting preliminary setup"
        check_system_specifications
        if ! set_keyboard_layout; then
            log_warn "Failed to set keyboard layout, continuing with default"
        fi
        check_uefi_mode
        check_internet_connection
        update_system_clock
        update_install_step 1
        log_debug "Preliminary setup completed"
    else
        log_info "Preliminary setup already completed, running checks again..."
        check_uefi_mode
        check_internet_connection
    fi
}