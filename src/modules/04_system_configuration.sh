#!/usr/bin/env bash
# src/modules/06_system_configuration.sh

set_timezone() {
    local timezone
    timezone=$(gum input --placeholder "Enter your timezone (e.g., America/New_York)")
    run_command arch-chroot /mnt ln -sf "/usr/share/zoneinfo/$timezone" /etc/localtime
    run_command arch-chroot /mnt hwclock --systohc
    log_info "Timezone set to $timezone"
}

set_locale() {
    local locale
    locale=$(gum input --placeholder "Enter your locale (e.g., en_US.UTF-8)")
    run_command sed -i "s/^#$locale/$locale/" /mnt/etc/locale.gen
    run_command arch-chroot /mnt locale-gen
    echo "LANG=$locale" | run_command tee /mnt/etc/locale.conf
    log_info "Locale set to $locale"
}

set_hostname() {
    local hostname
    hostname=$(gum input --placeholder "Enter your desired hostname")
    echo "$hostname" | run_command tee /mnt/etc/hostname
    log_info "Hostname set to $hostname"
}

configure_hosts() {
    local hostname
    hostname=$(cat /mnt/etc/hostname)
    cat << EOF | run_command tee /mnt/etc/hosts
127.0.0.1   localhost
::1         localhost
127.0.1.1   $hostname.localdomain $hostname
EOF
    log_info "Hosts file configured"
}

create_admin_user() {
    local username
    username=$(gum input --placeholder "Enter username for the admin user")
    run_command arch-chroot /mnt useradd -m -G wheel "$username"
    log_info "Setting password for $username"
    run_command arch-chroot /mnt passwd "$username"
    log_info "User $username created with admin privileges"
}

configure_sudo() {
    run_command sed -i '/%wheel ALL=(ALL:ALL) ALL/s/^# //' /mnt/etc/sudoers
    log_info "Sudo access granted to wheel group"
}

disable_root_login() {
    run_command arch-chroot /mnt passwd -l root
    log_info "Root login disabled"
}

configure_pacman_and_makepkg() {
    # Enable color in pacman
    run_command sed -i 's/^#Color/Color/' /mnt/etc/pacman.conf

    # Enable parallel downloads
    run_command sed -i 's/^#ParallelDownloads/ParallelDownloads/' /mnt/etc/pacman.conf

    # Configure makeflags
    local core_count
    core_count=$(nproc)
    echo "MAKEFLAGS=\"-j$core_count\"" | run_command tee -a /mnt/etc/makepkg.conf

    # Change compression settings (example: using zstd)
    run_command sed -i 's/COMPRESSXZ=(xz -c -z -)/COMPRESSXZ=(xz -c -T 0 -)/' /mnt/etc/makepkg.conf
    echo 'COMPRESSZST=(zstd -c -T0 -)' | run_command tee -a /mnt/etc/makepkg.conf

    log_info "Pacman and makepkg configured"
}

enable_network_manager() {
    log_info "Enabling NetworkManager service"
    run_command arch-chroot /mnt systemctl enable NetworkManager
    log_info "NetworkManager service enabled"
}

configure_bootloader() {
    log_info "Configuring GRUB bootloader"

    # Install GRUB
    run_command arch-chroot /mnt pacman -S --noconfirm grub efibootmgr

    # Install GRUB to the EFI system partition
    run_command arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB

    # Generate GRUB configuration
    run_command arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

    log_info "GRUB bootloader configured successfully"
}

configure_firewall() {
    log_info "Configuring firewall (ufw)"

    # Install ufw if not already installed
    run_command arch-chroot /mnt pacman -S --noconfirm ufw

    # Enable ufw
    run_command arch-chroot /mnt systemctl enable ufw

    # Set default policies
    run_command arch-chroot /mnt ufw default deny incoming
    run_command arch-chroot /mnt ufw default allow outgoing

    # Allow SSH (Optional)
    # run_command arch-chroot /mnt ufw allow ssh

    # Enable the firewall
    run_command arch-chroot /mnt ufw enable

    log_info "Firewall (ufw) configured and enabled"
}

enable_time_sync() {
    log_info "Enabling time synchronization"

    # Enable systemd-timesyncd service
    run_command arch-chroot /mnt systemctl enable systemd-timesyncd

    # Set fallback NTP servers (optional)
    echo "NTP=0.arch.pool.ntp.org 1.arch.pool.ntp.org 2.arch.pool.ntp.org 3.arch.pool.ntp.org" | run_command tee -a /mnt/etc/systemd/timesyncd.conf
    echo "FallbackNTP=0.pool.ntp.org 1.pool.ntp.org 2.pool.ntp.org 3.pool.ntp.org" | run_command tee -a /mnt/etc/systemd/timesyncd.conf

    log_info "Time synchronization enabled"
}

setup_pacman_hooks() {
    log_info "Setting up pacman hooks"

    # Create the hooks directory
    run_command mkdir -p /mnt/etc/pacman.d/hooks

    # Determine installed kernels
    local installed_kernels=""
    for kernel in linux linux-lts linux-zen linux-hardened; do
        if run_command arch-chroot /mnt pacman -Q "$kernel" &>/dev/null; then
            installed_kernels+="Target = $kernel"$'\n'
        fi
    done

    # Determine installed microcode
    local installed_microcode=""
    if run_command arch-chroot /mnt pacman -Q amd-ucode &>/dev/null; then
        installed_microcode="Target = amd-ucode"
    elif run_command arch-chroot /mnt pacman -Q intel-ucode &>/dev/null; then
        installed_microcode="Target = intel-ucode"
    fi

    # Create a hook to update GRUB when the kernel or microcode is updated
    cat << EOF | run_command tee /mnt/etc/pacman.d/hooks/95-update-grub.hook
[Trigger]
Type = Package
Operation = Upgrade
${installed_kernels}${installed_microcode}

[Action]
Description = Updating GRUB config
When = PostTransaction
Exec = /usr/bin/grub-mkconfig -o /boot/grub/grub.cfg
EOF

    log_info "Pacman hooks set up successfully"
}

system_configuration() {
    ui_section "System Configuration"

    local current_step
    current_step=$(get_install_step)

    if [[ "$current_step" -lt 4 ]]; then
        set_timezone
        set_locale
        set_hostname
        configure_hosts
        create_admin_user
        configure_sudo
        disable_root_login
        configure_pacman_and_makepkg
        enable_network_manager
        configure_bootloader
        configure_firewall
        enable_time_sync
        setup_pacman_hooks
        update_install_step 4
    else
        log_info "System configuration already completed, skipping..."
    fi

    log_info "System configuration completed successfully"
}