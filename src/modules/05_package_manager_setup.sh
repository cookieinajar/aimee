#!/usr/bin/env bash
# src/modules/05_package_manager_setup.sh

choose_aur_helper() {
    log_info "Choosing AUR helper"
    local aur_helper
    aur_helper=$(gum choose "paru" "yay")
    echo "$aur_helper"
}

install_aur_helper() {
    local aur_helper="$1"
    local username
    username=$(get_config "admin_username")

    log_info "Installing $aur_helper AUR helper"
    run_command arch-chroot /mnt bash -c "
        cd /tmp
        sudo -u $username git clone https://aur.archlinux.org/$aur_helper.git
        cd $aur_helper
        sudo -u $username makepkg -si --noconfirm
        cd ..
        rm -rf $aur_helper
    "
    log_info "$aur_helper installed successfully"
}

add_chaotic_aur() {
    log_info "Adding ChaoticAUR repository"
    run_command arch-chroot /mnt bash -c "
        pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
        pacman-key --lsign-key 3056513887B78AEB
        pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst'
        pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'"
    echo "[chaotic-aur]
Include = /etc/pacman.d/chaotic-mirrorlist" | run_command tee -a /mnt/etc/pacman.conf
    log_info "ChaoticAUR added successfully"
}

install_flatpak() {
    log_info "Installing Flatpak"
    run_command arch-chroot /mnt pacman -S --noconfirm flatpak
    run_command arch-chroot /mnt flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    log_info "Flatpak installed and Flathub repository added"
}

enable_multilib_repo() {
    log_info "Enabling multilib repository"
    run_command sed -i "/\[multilib\]/,/Include/s/^#//" /mnt/etc/pacman.conf
    run_command arch-chroot /mnt pacman -Sy
    log_info "Multilib repository enabled"
}

package_manager_setup() {
    ui_section "Package Manager Setup"

    local current_step
    current_step=$(get_install_step)

    if [[ "$current_step" -lt 5 ]]; then
        # AUR helper setup
        local aur_helper
        aur_helper=$(choose_aur_helper)
        install_aur_helper "$aur_helper"
        save_config "aur_helper" "$aur_helper"

        # Flatpak setup
        if gum confirm "Do you want to install Flatpak?"; then
            install_flatpak
        fi

        # Enable multilib repository
        enable_multilib_repo

        update_install_step 5
    else
        log_info "Package manager setup already completed, skipping..."
    fi

    log_info "Package manager setup completed successfully"
}