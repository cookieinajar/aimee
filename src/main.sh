#!/usr/bin/env bash
# src/main.sh

set -euo pipefail

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly SCRIPT_DIR
readonly VERSION="1.0.0"

# Default values
export LOG_LEVEL=${LOG_LEVEL:-INFO}
export LOG_FILE=${LOG_FILE:-"/var/log/arch_install.log"}

source_file() {
    local file="$1"
    if [[ ! -f "$file" ]]; then
        echo "Error: $file not found" >&2
        exit 1
    fi
    # shellcheck source=/dev/null
    source "$file"
}

source_utils_and_modules() {
    local utils=("logger.sh" \
                 "ui.sh" \
                 "command_utils.sh")

    local modules=("00_configuration.sh" \
                   "01_preliminary_setup.sh" \
                   "02_disk_setup.sh")

    for util in "${utils[@]}"; do
        source_file "${SCRIPT_DIR}/utils/${util}"
    done

    for module in "${modules[@]}"; do
        source_file "${SCRIPT_DIR}/modules/${module}"
    done
}

print_usage() {
    cat << EOF
Usage: $0 [options]

Options:
  --loglevel LEVEL   Set the log level (DEBUG, INFO, WARN, ERROR, FATAL)
  --logfile FILE     Specify the log file (default: $LOG_FILE)
  --help             Show this help message and exit
  --version          Show version information and exit

EOF
}

parse_args() {
    while [[ $# -gt 0 ]]; do
        case $1 in
            --loglevel)
                if [[ -n "${2:-}" ]]; then
                    LOG_LEVEL="${2^^}"
                    shift 2
                else
                    echo "Error: Log level not specified" >&2
                    exit 1
                fi
                ;;
            --logfile)
                if [[ -n "${2:-}" ]]; then
                    LOG_FILE="$2"
                    shift 2
                else
                    echo "Error: Log file not specified" >&2
                    exit 1
                fi
                ;;
            --help)
                print_usage
                exit 0
                ;;
            --version)
                echo "AIMEE v${VERSION}"
                exit 0
                ;;
            *)
                echo "Error: Unknown option: $1" >&2
                print_usage
                exit 1
                ;;
        esac
    done
}

main() {
    parse_args "$@"

    if [[ $EUID -ne 0 ]]; then
        echo "Error: This script must be run as root" >&2
        exit 1
    fi

    source_utils_and_modules

    local start_time end_time duration
    start_time=$(date +%s)

    ui_welcome

    log_info "Starting Arch Linux installation"
    log_debug "Log level set to $LOG_LEVEL"
    log_debug "Log file set to $LOG_FILE"    

    init_config
    preliminary_setup
    disk_setup

    ui_installation_complete
    
    end_time=$(date +%s)
    duration=$((end_time - start_time))
    log_info "Installation completed successfully in $duration seconds"
}

trap 'log_error "Installation interrupted"; exit 1' INT TERM

main "$@"